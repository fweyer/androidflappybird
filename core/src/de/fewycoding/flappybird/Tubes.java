package de.fewycoding.flappybird;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;

public class Tubes {
    private final Texture toptube;
    private final Texture bottomtube;
    float tubeWidht;
    float toptubeheight; //1100 optimal
    float bottumtubeheight;  // -700 optimal
    private final int random;
    int score;
    boolean isNotScored;

    public Tubes(float tubeWidht){
        int randompositive =  (int)(Math.random()*100)*6;
        int randomnegative = -(int)((Math.random()*100)*5);
        this.random = randomnegative + randompositive;
        this.tubeWidht = tubeWidht;
        this.toptube = new Texture("toptube.png");
        this.bottomtube = new Texture("bottomtube.png");
        this.toptubeheight = 1100+ this.random;
        this.bottumtubeheight = -700 + this.random;
        this.isNotScored = true;
    }

    public Texture getToptube() {
        return toptube;
    }

    public Texture getBottomtube() {
        return bottomtube;
    }

    public float getTubeWidht() {
        return tubeWidht;
    }

    public float getToptubeheight() {
        return toptubeheight;
    }

    public float getBottumtubeheight() {
        return bottumtubeheight;
    }

    //teilt der Tube einen Zustand mit das Sie gescoret wurde. Damit in der Fontklasse der Score übernommen werden kann
    public void scoringTube (Fonts font){
        if((tubeWidht< Gdx.graphics.getWidth()/3)&& isNotScored){
            this.score = 1;
            isNotScored = false;
            font.setScoreOneUp();
            font.setLevel();
        }

    }
}




