package de.fewycoding.flappybird;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;


public class FlappyBird extends ApplicationAdapter {
	SpriteBatch batch;
	Texture img;
	Bird bird;
	float distandce = 1100;
	Tubes[] numberTubes = null;
	Circle circle = null;
	Rectangle rectangletop;
	Rectangle rectanglebottom;
	Fonts font;

	// Die create Mehode instanziert die Objekte. Ist von LibGDX so vorgegeben.
	@Override
	public void create() {
		batch = new SpriteBatch();
		img = new Texture("bg.png");
		bird = new Bird();
		circle = new Circle();
		font = new Fonts();
		rectangletop = new Rectangle();
		rectanglebottom = new Rectangle();
		numberTubes = new Tubes[4];
		numberTubes[0] = new Tubes(1000);
		numberTubes[1] = new Tubes(1500);
		numberTubes[2] = new Tubes(2000);
		numberTubes[3] = new Tubes(2500);
	}

	@Override

		// die Rendermethode zeichnet das Spiel und wird jede Sekunde wieder neu ausgeführt
	public void render() {
		// Diese Anweisung lässt den Vogel erstmal gerade fliegen. Nach erstem drücken kann er aufsteigen und abstürzen.
		bird.startJourny();
		//legt einen virtuellen Kreis um den Vogel, damit dieser später Kollision erkennt.
		circle.set(bird.getBirdwidht() + 70, bird.getBirdHeight() + 40, 65.0f);


		//Zeichnet den Vogel und den Hintergrund
		batch.begin();
		batch.draw(img, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		batch.draw(bird.getBirdimage()[bird.getFlapstate()], bird.getBirdwidht(), bird.getBirdHeight());

		//Zeichnet die Tubes die if Anweisung regelt die Kollision
		for (int i = 0; i < 4; i++) {
			//zeichnt die Röhren und legt um diese ein virtuelles Rechteck
			batch.draw(numberTubes[i].getToptube(), numberTubes[i].tubeWidht -= font.level, numberTubes[i].toptubeheight+=font.gamestate);
			batch.draw(numberTubes[i].getBottomtube(), numberTubes[i].tubeWidht -= font.level, numberTubes[i].bottumtubeheight+=font.gamestate);
			rectangletop.set(numberTubes[i].tubeWidht, numberTubes[i].getToptubeheight(), 200, 1700);
			rectanglebottom.set(numberTubes[i].tubeWidht, numberTubes[i].getBottumtubeheight(), 200, 1275);
			//regelt den Score im Spiel
			numberTubes[i].scoringTube(font);

			//regelt die Kollesion. Der boole isnotgameover sorg dafür das der Vogel nicht mehr fliegen aufsteigen kann.
			// Der Vogel fliegt kleiner 0 und das Spiel ist beeendet
			if (Intersector.overlaps(circle, rectangletop) || Intersector.overlaps(circle, rectanglebottom)) {
				System.out.println("kollision");
				bird.setIsnotGameover(false);
			}

			// Erstelllt eine neue Tube im rechten Bildschirm mit einer Distandce von aktuell 1100.
			// von 0-1000 ist der Bildschirm sichtbar. Die Tube wird gelöscht wenn diese den Wert in der Breite kleiner
			// 0 erreicht.
			if (numberTubes[i].getTubeWidht()< -Gdx.graphics.getWidth()) {
				numberTubes[i] = new Tubes(distandce);
			}

			//gibt x und y koordinaten aus.
			//font.creatteYandX(batch,Gdx.input.getX(),Gdx.input.getY());


			//Zeichnet den PunkteScore
			font.createScore(batch);

			}
			batch.end();
		}

	//Entsorgt die Objekte
	@Override
	public void dispose(){
		batch.dispose();
		img.dispose();
		bird.getBirdimage()[0].dispose();
		bird.getBirdimage()[1].dispose();
	}
}

// Notizen der Shaperenderer zeichnet einen Kreis bzw. ein Rechteck um meinene Spielfiguren. BRaucht man zur Kollisions
// erkennt.
// Random -450 to
//shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
//shapeRenderer.setColor(Color.RED);
//shapeRenderer.end();
   
//shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
//shapeRenderer.rect(numberTubes[i].tubeWidht, numberTubes[i].getToptubeheight(),200, 1700);
//shapeRenderer.rect(numberTubes[i].tubeWidht, numberTubes[i].getBottumtubeheight(),200 , 1275);
//shapeRenderer.setColor(Color.RED);
//shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
//shapeRenderer.circle(bird.getBirdwidht()+70,bird.getBirdHeight()+40,65.0f);
//shapeRenderer.setColor(Color.RED);
//shapeRenderer.end();*/