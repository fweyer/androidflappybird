package de.fewycoding.flappybird;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class Fonts {

    private Integer score = 0;
    private final BitmapFont fontscore;
    Float level = 1.5f;
    float gamestate = 0;


    public Fonts() {
        this.fontscore = new BitmapFont();
    }

    public void createScore(SpriteBatch batch) {
        fontscore.setColor(Color.BLUE);
        fontscore.getData().setScale(8);
        fontscore.draw(batch, score.toString(), 150, 200);
        //fontscore.draw(batch, this.level.toString(),150,300);
    }

    public void setScoreOneUp() {
        this.score++;
    }

    public void setLevel(){
        this.level +=0.2f;
        if(this.level>4 && gamestate==0){
            this.level =2.0f;
            gamestate = 1;
        }

        if(this.level>4.1){
            gamestate = -1;
        }

        if(this.level>6){
            this.level = 1.5f;
            gamestate = 0;
        }

    }

}
