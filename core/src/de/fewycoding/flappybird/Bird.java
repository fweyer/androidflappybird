package de.fewycoding.flappybird;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;

public class Bird {
    private float gravity;
    private int flapstate;
    private float birdHeight;
    private final float birdwidht;
    private float velocity;
    private boolean flying = false;
    private boolean isnotGameover = true;
    private final Texture[] birdimage = new Texture[2];


    public Bird() {
        this.gravity = 0;
        this.flapstate = 0;
        this.birdHeight = Gdx.graphics.getHeight() / 2;
        this.velocity = 0.3f;
        this.birdimage[0] = new Texture ("bird.png");
        this.birdimage[1] = new Texture("bird2.png");
        this.birdwidht = (Gdx.graphics.getWidth() / 2)-150;
    }

    public void isflying(boolean isnotGameover) {
        if (isnotGameover) {
            if (Gdx.input.justTouched()) {
                for (int i = 0; i < 15000; i++) {
                    aufsteigen();
                }
                this.velocity = 0;
                this.gravity = 0;
            } else {
               this.birdfallingdown();
            }
        }
    }

    public void isCrasched() {

        if (this.birdHeight < 0 || this.birdHeight > 1700) {
            Gdx.app.exit();
        }
    }

    public void isflappstate(){
        if(this.flapstate==0)
        {this.flapstate=1;}
        else{this.flapstate=0;}
    }

    public int getFlapstate() {
        return flapstate;
    }


    private void aufsteigen(){
        this.birdHeight += 0.01f;
    }


    public float getBirdwidht() {
        return birdwidht;
    }

    public float getBirdHeight() {
        return birdHeight;
    }

    public Texture[] getBirdimage() {
        return birdimage;
    }

    public void birdfallingdown(){
            this.birdHeight -= this.velocity + this.gravity;
            this.gravity += 0.5f;
    }

    public void startJourny(){
        if (Gdx.input.isTouched()|| flying) {
            this.isCrasched();
            this.isflying(this.isnotGameover);
            this.flying = true;
            this.isflappstate();
        }

        if(!this.isnotGameover){
            this.birdfallingdown();
        }
    }

    public void setIsnotGameover(boolean isnotGameover) {
        this.isnotGameover = isnotGameover;
    }
}


