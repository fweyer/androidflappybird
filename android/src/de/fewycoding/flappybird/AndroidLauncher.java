package de.fewycoding.flappybird;

import android.os.Bundle;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import de.academy.flappybird.FlappyBird;

public class AndroidLauncher extends AndroidApplication {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		initialize(new FlappyBird(), config);
	}

	/*
	* Beschreibung des Spiels.
	* Ich habe einen FlappyBird entwickelt. Ziel des Spiels ist es mit dem Vogel durch die Röhren zu fliegen.
	* Dabei bekommt der Spieler Punkte jedes mal wenn er eine Röhre überquert. Das Spiel wird je mehr Punkte man
	* generiert schwieriger, da sich die Röhren schneller von rechts nach links bewegen. Ich habe eine Bird Klasse
	* die sich um die Physik des Vogels kümmert. Eine TubesKlasse die mir die Tubes generiert mit einer Methode um mit der
	* Fontklasse bezüglich des Scores zu kommunizieren. Die FlappyBird Klasse ist die Main-Klasse. Die Fontklasse regelt
	* die Darstellung der Punkte, des Levels und der Y und X koordinaten wo der Benutzer drauf drückt. Ich habe das
	* Framework LibGDX zur erstellung des Spiels verwendet.
	*
	* */






}

